import csv
from collections import OrderedDict
import matplotlib.pyplot as plt
from pprint import pprint


def matches_played_per_year():
    """
    matches_played = {
        season: num_of_matches
    }
    """

    matches_played = {}

    with open('static/matches.csv') as file:
        data_dict = csv.DictReader(file)

        # check for existance of the year
        for data in data_dict:
            if data['season'] in matches_played:
                matches_played[data['season']] += 1
            else:
                matches_played[data['season']] = 1

    pprint(matches_played)

    matches_played = OrderedDict(sorted(matches_played.items(), key=lambda tup: tup[0]))
    return matches_played


def plot_graph(matches_played):
    plt.title("Matches Played Per Year")
    plt.figure(1, figsize=(10, 6))
    plt.bar(matches_played.keys(), matches_played.values())
    plt.xlabel("YEARS")
    plt.ylabel("MATCHES")

   
    plt.show()


def main():
    matches_played = matches_played_per_year()
    plot_graph(matches_played)


main()
