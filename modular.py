from collections import OrderedDict


def matches_played_per_year(matches_data):
    """
    matches_played = {
        season: num_of_matches
    }
    """
    
    matches_played = {}


    for data in matches_data:
        if data['season'] in matches_played:
            matches_played[data['season']] += 1
        else:
            matches_played[data['season']] = 1

    print("""
    ==============================
    Years : matches playes    
    """)

    
    return(matches_played)

###################################################################
# Teams Won per year

def get_teams_won(matches_data):
    teams_won = {}
    for data in matches_data:       
        year = data["season"]
        winner = data["winner"]

        if winner:
            if year not in teams_won:
                teams_won[year] = {}

            if winner in teams_won[year]:
                teams_won[year][winner] += 1
            else:
                teams_won[year][winner] = 1

    return teams_won

    
##################################################
# Extra runs given by each Team

def get_match_id(year, matches_data):
    match_ids = []

    for data in matches_data:
        if data["season"] == str(year):
            match_ids.append(data["id"])
    return match_ids


def extra_runs_conceded(match_ids, deliveries_data):
    """
    extra_runs = {
        team_names: extra_runs_given
    }
    """
    extra_runs = {}

        # get extra runs given by bowling team matching the match id
    for deliveries in deliveries_data:
        if deliveries["match_id"] in match_ids and deliveries["extra_runs"] is not "0":
            if deliveries["bowling_team"] in extra_runs:
                extra_runs[deliveries["bowling_team"]] += int(deliveries["extra_runs"])
            else:
                extra_runs[deliveries["bowling_team"]] = int(deliveries["extra_runs"])

    return extra_runs

def extra_runs(matches_data, deliveries_data):
    match_ids = get_match_id(2016, matches_data)
    extra_runs = extra_runs_conceded(match_ids, deliveries_data)
    print("\n \n")
    print("Teams : Extra runs given")
    print("\n")
    return extra_runs


####################################################
# top ten economy bowler



# find total runs and total bowl bowled by each bowler
def cal_top_eco(match_ids, deliveries_data):

    """
    bowlers_performance = {

        bowler_name: [total_runs, bowls bowled]
    }
    """

    bowlers_performance = {}

    for deliveries in deliveries_data:
        if deliveries["match_id"] in match_ids:
            total_runs = int(deliveries["total_runs"])
            bowler = deliveries["bowler"]

            bowl = 0

            #check if valid bowl
            if deliveries["wide_runs"] == '0' and deliveries["noball_runs"] == '0':
                bowl = 1
            
            if bowler in bowlers_performance:
                bowlers_performance[bowler][0] += total_runs
                bowlers_performance[bowler][1] += bowl
            else:
                bowlers_performance[bowler] = [total_runs, bowl]  

    return bowlers_performance


def bowler_economy(bowlers_performance):
    economy = {}

    for k,v in bowlers_performance.items():
        eco = round(v[0]/((v[1])/6),2)
        economy[k] = eco

    sort_eco = OrderedDict(sorted(economy.items(), key = lambda kv: kv[1])[:10])
    return sort_eco

def top_eco(matches_data, deliveries_data):
    match_ids = get_match_id(2015, matches_data)
    bowlers_performance = cal_top_eco(match_ids, deliveries_data)
    economy = bowler_economy(bowlers_performance)
    return economy