import modular
import csv
from pprint import pprint

# matches_data = csv.DictReader(open("static/matches.csv"))
# deliveries_data = csv.DictReader(open("static/deliveries.csv"))

def get_info():

    option = input("""
    \t \t \t \tWelcome To IPL info Portal
    \t \t \t \t==========================

    Select the option below to get the required info

    --> select 1 to get total matches played

    --> select 2 to get total number of matches won by each team per year

    --> select 3 to get extra runs given by each team in 2015

    --> select 4 to get top 10 economy players in 2016

    --> type 0 to exit
    """)

    # while(option):
    # func_dict = {
    #     "1": modular.matches_played_per_year(matches_data),
    #     "2": modular.get_teams_won(matches_data),
    #     # "3": extra_runs,
    #     # "4": top_eco
    # }

    while(option):
        try:
            matches_data = csv.DictReader(open("static/matches.csv"))
            deliveries_data = csv.DictReader(open("static/deliveries.csv"))
        except:
            print("File not Found")
            break

        print("option  ", option)
        print("\n")
        if option == "1":
            pprint(modular.matches_played_per_year(matches_data))
            print("\n")
            print("=====================================================================")

        elif option == "2":
            pprint(modular.get_teams_won(matches_data))
            print("\n")
            print("=====================================================================")

        elif option == "3":
            pprint(modular.extra_runs(matches_data, deliveries_data))
            print("\n")
            print("=====================================================================")

        elif option == "4":
            result = modular.top_eco(matches_data, deliveries_data)
            print("\n")
            print("name --> economy")
          
            for name, eco in result.items():
                print("\n")
                print("{0} --> {1}".format(name,eco))
            
            print("\n")
            print("=====================================================================")

        elif option == "0":
            break

        else:
            print("Not a valid option")

        option = input("Enter option")



get_info()