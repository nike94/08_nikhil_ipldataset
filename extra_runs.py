import csv
from pprint import pprint
import matplotlib.pyplot as plt


def get_match_id(year):
    match_ids = []

    with open('static/matches.csv') as file:
        data_dict = csv.DictReader(file)

        for data in data_dict:
            if data["season"] == str(year):
                match_ids.append(data["id"])
    return match_ids


def extra_runs_conceded(match_ids):
    """
    extra_runs = {
        team_names: extra_runs_given
    }
    """
    extra_runs = {}

    with open('static/deliveries.csv') as file:
        deliveries_dict = csv.DictReader(file)

        # get extra runs given by bowling team matching the match id
        for deliveries in deliveries_dict:
            if deliveries["match_id"] in match_ids and deliveries["extra_runs"] is not "0":
                if deliveries["bowling_team"] in extra_runs:
                    extra_runs[deliveries["bowling_team"]] += int(deliveries["extra_runs"])
                else:
                    extra_runs[deliveries["bowling_team"]] = int(deliveries["extra_runs"])

    pprint(extra_runs)
    return extra_runs


def polt_graph(extra_runs):
    team_short_name = {
        'Delhi Daredevils': "DD",
        'Gujarat Lions': "GL",
        'Kings XI Punjab': "KXIP",
        'Kolkata Knight Riders': "KKR",
        'Mumbai Indians': "MI",
        'Rising Pune Supergiants': "RPS",
        'Royal Challengers Bangalore': "RCB",
        'Sunrisers Hyderabad': "SH"
    }

    team_names = []
    runs = []

    for team in extra_runs:
        team_names.append(team_short_name[team])
        runs.append(extra_runs[team])
    plt.title("Extra Runs Conceded")
    plt.figure(1, figsize=(100, 6))
    plt.bar(team_names, runs, width=0.5)
    plt.ylabel("RUNS")
    plt.xlabel("TEAMS")

    plt.show()


def main():
    match_ids = get_match_id(2016)
    extra_runs = extra_runs_conceded(match_ids)
    polt_graph(extra_runs)


main()
