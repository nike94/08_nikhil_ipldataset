import csv
from pprint import pprint
from collections import OrderedDict
import matplotlib.pyplot as plt


def get_match_id(year):
    match_ids = []

    with open('static/matches.csv') as file:
        data_dict = csv.DictReader(file)

        for data in data_dict:
            if data["season"] == str(year):
                match_ids.append(data["id"])
    
    return match_ids


# find total runs and total bowl bowled by each bowler
def cal_top_eco(match_ids):

    """
    bowlers_performance = {

        bowler_name: [total_runs, bowls bowled]
    }
    """

    bowlers_performance = {}

    with open('static/deliveries.csv') as file:
        deliveries_dict = csv.DictReader(file)

        for deliveries in deliveries_dict:
            if deliveries["match_id"] in match_ids:
                total_runs = int(deliveries["total_runs"])
                bowler = deliveries["bowler"]

                bowl = 0

                #check if valid bowl
                if deliveries["wide_runs"] == '0' and deliveries["noball_runs"] == '0':
                    bowl = 1
                
                if bowler in bowlers_performance:
                    bowlers_performance[bowler][0] += total_runs
                    bowlers_performance[bowler][1] += bowl
                else:
                    bowlers_performance[bowler] = [total_runs, bowl]  

    return bowlers_performance


def bowler_economy(bowlers_performance):
    economy = {}

    for k,v in bowlers_performance.items():
        eco = round(v[0]/((v[1])/6),2)
        economy[k] = eco

    sort_eco = OrderedDict(sorted(economy.items(), key = lambda kv: kv[1])[:10])
    pprint(sort_eco)
    return(sort_eco)

    
def polt_graph(top_economy):
    plt.title("Top Ten Economy Bowler")
    plt.figure(1, figsize=(10, 6))
    plt.bar(top_economy.keys(), top_economy.values(), width=0.6)
    plt.xlabel("Bowlers")
    plt.ylabel("Economy Rate")

    plt.show()


def main():
    match_ids = get_match_id(2015)
    bowlers_performance = cal_top_eco(match_ids)
    economy = bowler_economy(bowlers_performance)
    polt_graph(economy)


main()