import csv
import matplotlib.pyplot as plt
from collections import OrderedDict
from pprint import pprint


def get_teams_won():
    teams = set()
    teams_won = {}

    with open('static/matches.csv') as file:

        data_dict = csv.DictReader(file)

        for data in data_dict:
            teams.add(data["team1"])
            year = data["season"]
            winner = data["winner"]
            if winner:
                if year not in teams_won:
                    teams_won[year] = {}

                if winner in teams_won[year]:
                    teams_won[year][winner] += 1
                else:
                    teams_won[year][winner] = 1

    pprint(teams_won)

    # adding all teams in all year
    for year in teams_won:
        teams_in_season = teams_won[year].keys()
        for team in teams:
            if team not in teams_in_season:
                teams_won[year][team] = "0"

    teams_won = OrderedDict(sorted(teams_won.items(), key=lambda kv: kv[0]))

    # sort all data
    for year in teams_won:
        teams_won[year] = OrderedDict(sorted(teams_won[year].items(), key=lambda kv: kv[0]))

    return teams_won


def plot_graph(teams_won):
    colors = {'Chennai Super Kings': 'y', 'Deccan Chargers': 'Teal', 'Delhi Daredevils': 'Maroon',
            'Gujarat Lions': 'Aqua', 'Kings XI Punjab': 'r', 'Kochi Tuskers Kerala': 'c',
            'Kolkata Knight Riders': 'Navy', 'Mumbai Indians': 'b', 'Pune Warriors': 'g',
            'Rajasthan Royals': 'Silver', 'Rising Pune Supergiant': 'Purple',
            'Royal Challengers Bangalore': 'Lime', 'Sunrisers Hyderabad': 'm'}
    key = sorted(colors.keys())

    for year in teams_won:
        bottom = 0
        for team, matches_won in teams_won[year].items():
            plt.bar(year, int(matches_won), bottom=bottom, color=colors[team])
            bottom += int(matches_won)

    plt.xlabel("YEARS")
    plt.ylabel("MATCHES WON")
    plt.legend(key, fontsize = "xx-small")
    plt.show()


def main():
    teams_won_per_year = get_teams_won()
    plot_graph(teams_won_per_year)


main()